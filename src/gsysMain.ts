/*---------------------------------------------------------
 * See LICENSE in the project root for license information.
 *--------------------------------------------------------*/

'use strict';

import vscode = require('vscode');
import fs = require('fs');
import path = require('path');

import { initGsysSuggest, closeAllGsysSuggestProcesses } from './gsysSuggestExec';
import { GsysCompletionItemProvider } from './gsysSuggest';
import { GsysDefinitionProvider } from './gsysDeclaration';
import { GsysReferenceProvider } from './gsysReferences';
import { GsysHoverProvider } from './gsysHover';
import { GsysRenameProvider } from './gsysRename';
import { GsysDocumentSymbolProvider, GsysWorkspaceSymbolProvider } from './gsysOutline';
import * as indexer from './gsysIndexer';
import { GsysSignatureHelpProvider } from './gsysSignature';
import { GsysFormattingProvider } from './gsysFormatting';
import { check, execSelectionInTerminal, activateEvalConsole } from './gsysBuild';
import { NIM_MODE } from './gsysMode';
import { showHideStatus } from './gsysStatus';
import { getDirtyFile, outputLine } from './gsysUtils';
import { ProgressLocation } from 'vscode';
import { initImports, removeFileFromImports, addFileToImports } from './gsysImports';

let diagnosticCollection: vscode.DiagnosticCollection;
var fileWatcher: vscode.FileSystemWatcher;
var terminal: vscode.Terminal | undefined;

export function activate(ctx: vscode.ExtensionContext): void {
    let config = vscode.workspace.getConfiguration('gsys');

    vscode.commands.registerCommand('gsys.run.file', runFile);
    vscode.commands.registerCommand('gsys.check', runCheck);
    vscode.commands.registerCommand('gsys.execSelectionInTerminal', execSelectionInTerminal);

    if (vscode.workspace.getConfiguration('gsys').get('enableGsyssuggest') as boolean) {
        initGsysSuggest();
        ctx.subscriptions.push(vscode.languages.registerCompletionItemProvider(NIM_MODE, new GsysCompletionItemProvider(), '.', ' '));
        ctx.subscriptions.push(vscode.languages.registerDefinitionProvider(NIM_MODE, new GsysDefinitionProvider()));
        ctx.subscriptions.push(vscode.languages.registerReferenceProvider(NIM_MODE, new GsysReferenceProvider()));
        ctx.subscriptions.push(vscode.languages.registerRenameProvider(NIM_MODE, new GsysRenameProvider()));
        ctx.subscriptions.push(vscode.languages.registerDocumentSymbolProvider(NIM_MODE, new GsysDocumentSymbolProvider()));
        ctx.subscriptions.push(vscode.languages.registerSignatureHelpProvider(NIM_MODE, new GsysSignatureHelpProvider(), '(', ','));
        ctx.subscriptions.push(vscode.languages.registerHoverProvider(NIM_MODE, new GsysHoverProvider()));
        ctx.subscriptions.push(vscode.languages.registerDocumentFormattingEditProvider(NIM_MODE, new GsysFormattingProvider()));
    }

    diagnosticCollection = vscode.languages.createDiagnosticCollection('gsys');
    ctx.subscriptions.push(diagnosticCollection);

    vscode.languages.setLanguageConfiguration(NIM_MODE.language as string, {
        // @Note Literal whitespace in below regexps is removed
        onEnterRules: [
            {
                beforeText: /^(\s)*## /,
                action: { indentAction: vscode.IndentAction.None, appendText: '## '}
            },
            {
                beforeText: new RegExp(String.raw`
                    ^\s*
                    (
                        (case) \b .* :
                    )
                    \s*$
                `.replace(/\s+?/g, '')),
                action: {
                    indentAction: vscode.IndentAction.None
                }
            },
            {
                beforeText: new RegExp(String.raw`
                    ^\s*
                    (
                        (
                            (proc|macro|iterator|template|converter|func) \b .*=
                        )|(
                            (import|export|let|var|const|type) \b
                        )|(
                            [^:]+:
                        )
                    )
                    \s*$
                `.replace(/\s+?/g, '')),
                action: {
                    indentAction: vscode.IndentAction.Indent
                }
            },
            {
                beforeText: new RegExp(String.raw`
                ^\s*
                    (
                        (
                            (return|raise|break|continue) \b .*
                        )|(
                            (discard) \b
                        )
                    )
                    \s*
                `.replace(/\s+?/g, '')),
                action: {
                    indentAction: vscode.IndentAction.Outdent
                }
            }
        ],

        wordPattern: /(-?\d*\.\d\w*)|([^\`\~\!\@\#\%\^\&\*\(\)\-\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\?\s]+)/g,
    });

    vscode.window.onDidChangeActiveTextEditor(showHideStatus, null, ctx.subscriptions);

    vscode.window.onDidCloseTerminal((e: vscode.Terminal) => {
        if (terminal && e.processId === terminal.processId) {
            terminal = undefined;
        }
    });

    console.log(ctx.extensionPath);
    activateEvalConsole();
    indexer.initWorkspace(ctx.extensionPath);
    fileWatcher = vscode.workspace.createFileSystemWatcher('**/*.gsys');
    fileWatcher.onDidCreate((uri) => {
        if (config.has('licenseString')) {
            let path = uri.fsPath.toLowerCase();
            if (path.endsWith('.gsys') || path.endsWith('.gsyss')) {
                fs.stat(uri.fsPath, (err, stats) => {
                    if (stats && stats.size === 0) {
                        let edit = new vscode.WorkspaceEdit();
                        edit.insert(uri, new vscode.Position(0, 0), config['licenseString']);
                        vscode.workspace.applyEdit(edit);
                    }
                });
            }
        }
        addFileToImports(uri.fsPath);
    });

    fileWatcher.onDidDelete(uri => {
        removeFileFromImports(uri.fsPath);
    });

    ctx.subscriptions.push(vscode.languages.registerWorkspaceSymbolProvider(new GsysWorkspaceSymbolProvider()));

    startBuildOnSaveWatcher(ctx.subscriptions);

    if (vscode.window.activeTextEditor && !!vscode.workspace.getConfiguration('gsys')['lintOnSave']) {
        runCheck(vscode.window.activeTextEditor.document);
    }

    if (vscode.workspace.getConfiguration('gsys').get('enableGsyssuggest') as boolean) {
        if (config.has('gsyssuggestRestartTimeout')) {
            let timeout = config['gsyssuggestRestartTimeout'] as number;
            if (timeout > 0) {
                console.log('Reset gsyssuggest process each ' + timeout + ' minutes');
                global.setInterval(() => closeAllGsysSuggestProcesses(), timeout * 60000);
            }
        }
    }

    initImports();
    outputLine('[info] Extension Activated');
}


export function deactivate(): void {
    closeAllGsysSuggestProcesses();
    fileWatcher.dispose();
}

function runCheck(document?: vscode.TextDocument) {
    let config = vscode.workspace.getConfiguration('gsys');
    if (!document && vscode.window.activeTextEditor) {
        document = vscode.window.activeTextEditor.document;
    }

    function mapSeverityToVSCodeSeverity(sev: string) {
        switch (sev) {
            case 'Hint': return vscode.DiagnosticSeverity.Warning;
            case 'Error': return vscode.DiagnosticSeverity.Error;
            case 'Warning': return vscode.DiagnosticSeverity.Warning;
            default: return vscode.DiagnosticSeverity.Error;
        }
    }

    if (!document || document.languageId !== 'gsys' || document.fileName.endsWith('gsys.cfg')) {
        return;
    }

    var uri = document.uri;

    vscode.window.withProgress(
        {location: ProgressLocation.Window, cancellable: false, title: 'Gsys: check project...'},
        (progress) => check(uri.fsPath, config)
    ).then(errors => {
        diagnosticCollection.clear();

        let diagnosticMap: Map<string, vscode.Diagnostic[]> = new Map();
        var err: { [key: string]: boolean; } = {};
        errors.forEach(error => {
            if (!err[error.file + error.line + error.column + error.msg]) {
                let targetUri = error.file;
                let endColumn = error.column;
                if (error.msg.indexOf('\'') >= 0) {
                    endColumn += error.msg.lastIndexOf('\'') - error.msg.indexOf('\'') - 2;
                }
                let line = Math.max(0, error.line - 1);
                let range = new vscode.Range(line, Math.max(0, error.column - 1), line, Math.max(0, endColumn));
                let diagnostic = new vscode.Diagnostic(range, error.msg, mapSeverityToVSCodeSeverity(error.severity));
                let diagnostics = diagnosticMap.get(targetUri);
                if (!diagnostics) {
                    diagnostics = [];
                }
                diagnosticMap.set(targetUri, diagnostics);
                diagnostics.push(diagnostic);
                err[error.file + error.line + error.column + error.msg] = true;
            }
        });

        let entries: [vscode.Uri, vscode.Diagnostic[]][] = [];
        diagnosticMap.forEach((diags, uri) => {
            entries.push([vscode.Uri.file(uri), diags]);
        });
        diagnosticCollection.set(entries);
    });
}

function startBuildOnSaveWatcher(subscriptions: vscode.Disposable[]) {
    vscode.workspace.onDidSaveTextDocument(document => {
        if (document.languageId !== 'gsys') {
            return;
        }
        if (!!vscode.workspace.getConfiguration('gsys')['lintOnSave']) {
            runCheck(document);
        }
        if (!!vscode.workspace.getConfiguration('gsys')['buildOnSave']) {
            vscode.commands.executeCommand('workbench.action.tasks.build');
        }
    }, null, subscriptions);
}

function runFile() {
    let editor = vscode.window.activeTextEditor;
    if (editor) {
        if (!terminal) {
            terminal = vscode.window.createTerminal('Gsys');
        }
        terminal.show(true);
        if (editor.document.isUntitled) {
            terminal.sendText('gsys ' + vscode.workspace.getConfiguration('gsys')['buildCommand'] +
                ' -r "' + getDirtyFile(editor.document) + '"', true);
        } else {
            let outputDirConfig = vscode.workspace.getConfiguration('gsys')['runOutputDirectory'];
            var outputParams = '';
            if (!!outputDirConfig) {
                if (vscode.workspace.workspaceFolders) {
                    var rootPath = '';
                    for (const folder of vscode.workspace.workspaceFolders) {
                        if (folder.uri.scheme === 'file') {
                            rootPath = folder.uri.fsPath;
                            break;
                        }
                    }
                    if (rootPath !== '') {
                        if (!fs.existsSync(path.join(rootPath, outputDirConfig))) {
                            fs.mkdirSync(path.join(rootPath, outputDirConfig));
                        }
                        outputParams = ' --out:"' + path.join(outputDirConfig, path.basename(editor.document.fileName, '.gsys')) + '"';
                    }
                }
            }
            if (editor && editor.document.isDirty) {
                editor.document.save().then((success: boolean) => {
                    if (terminal && editor && success) {
                        terminal.sendText('gsys ' + vscode.workspace.getConfiguration('gsys')['buildCommand'] +
                            outputParams + ' -r "' + editor.document.fileName + '"', true);
                    }
                });
            } else {
                terminal.sendText('gsys ' + vscode.workspace.getConfiguration('gsys')['buildCommand'] +
                    outputParams + ' -r "' + editor.document.fileName + '"', true);
            }
        }
    }
}
