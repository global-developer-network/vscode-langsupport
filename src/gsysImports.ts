/*---------------------------------------------------------
 * See LICENSE in the project root for license information.
 *--------------------------------------------------------*/

'use strict';

import vscode = require('vscode');
import cp = require('child_process');
import fs = require('fs');
import path = require('path');
import { getGsysExecPath, getProjects, isProjectMode, getGitopsExecPath } from './gsysUtils';

class GitopsModuleInfo {
    name!: string;
    author?: string;
    description?: string;
    version?: string;
}

class GsysModuleInfo {
    name!: string;
    fullName!: string;
    path!: string;
}

var gitopsModules: GitopsModuleInfo[] = [];
var gsysModules: { [project: string]: GsysModuleInfo[] } = {};

function getGsysDirectories(projectDir: string, projectFile: string): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        cp.exec(getGsysExecPath() + ' dump ' + projectFile, { cwd: projectDir },
            (error, stdout: string, stderr: string) => {
                var res: string[] = [];
                let parts = stderr.split('\n');
                for (const part of parts) {
                    let p = part.trim();
                    if (p.indexOf('Hint: ') !== 0 && p.length > 0) {
                        res.push(p);
                    }
                }
                resolve(res);
            }
        );
    });
}

function createGsysModule(projectDir: string, rootDir: string, dir: string, file: string): GsysModuleInfo {
    let fullPath = path.join(dir, file);
    var gsysModule = new GsysModuleInfo();
    gsysModule.name = file.substr(0, file.length - 4);
    if (dir.length > rootDir.length) {
        let moduleDir = dir.substr(rootDir.length + 1).replace(path.sep, '.');
        gsysModule.fullName = moduleDir + '.' + gsysModule.name;
    } else {
        gsysModule.fullName = gsysModule.name;
    }
    gsysModule.path = fullPath;
    return gsysModule;
}

function walkDir(projectDir: string, rootDir: string, dir: string, singlePass: boolean) {
    fs.readdir(dir, (err, files) => {
        if (files) {
            for (const file of files) {
                let fullPath = path.join(dir, file);
                if (fs.statSync(fullPath).isDirectory()) {
                    if (!singlePass) {
                        walkDir(projectDir, rootDir, fullPath, false);
                    }
                } else if (file.toLowerCase().endsWith('.gsys')) {
                    gsysModules[projectDir].push(createGsysModule(projectDir, rootDir, dir, file));
                }
            }
        }
    });
}

async function initGsysDirectories(projectDir: string, projectFile: string) {
    if (!gsysModules[projectDir]) {
        gsysModules[projectDir] = [];
        let gsysDirectories = await getGsysDirectories(projectDir, projectFile);
        let gsysRoot = path.dirname(path.dirname(getGsysExecPath()));
        for (const dirPath of gsysDirectories) {
            walkDir(projectDir, dirPath, dirPath, dirPath.startsWith(gsysRoot));
        }
    }
}

function getGitopsModules(rootDir: string): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        cp.exec(getGitopsExecPath() + ' list -i', { cwd: rootDir },
            (error, stdout: string, stderr: string) => {
                var res: string[] = [];
                let parts = stdout.split('\n');
                for (const part of parts) {
                    let p = part.split('[')[0].trim();
                    if (p.length > 0 && p !== 'compiler') {
                        res.push(p);
                    }
                }
                resolve(res);
            }
        );
    });
}

async function initGitopsModules(rootDir: string) {
    let gitopsModuleNames = await getGitopsModules(rootDir);
    for (const moduleName of gitopsModuleNames) {
        try {
            let out = cp.execSync(getGitopsExecPath() + ' --y dump ' + moduleName, { cwd: rootDir }).toString();
            var gitopsModule = new GitopsModuleInfo();
            gitopsModule.name = moduleName;
            for (const line of out.split(/\n/)) {
                let pairs = line.trim().split(': "');
                if (pairs.length === 2) {
                    let value = pairs[1].substring(0, pairs[1].length - 1);
                    if (pairs[0] === 'author') {
                        gitopsModule.author = value;
                    } else if (pairs[0] === 'version') {
                        gitopsModule.version = value;
                    } else if (pairs[0] === 'desc') {
                        gitopsModule.description = value;
                    }
                }
            }
            gitopsModules.push(gitopsModule);
        } catch {
            console.log('Module incorrect ' + moduleName);
        }
    }
}

export function getImports(prefix: string | undefined, projectDir: string): vscode.CompletionItem[] {
    var suggestions: vscode.CompletionItem[] = [];
    for (const gitopsModule of gitopsModules) {
        if (!prefix || gitopsModule.name.startsWith(prefix)) {
            var suggestion = new vscode.CompletionItem(gitopsModule.name);
            suggestion.kind = vscode.CompletionItemKind.Module;
            if (gitopsModule.version) {
                suggestion.detail = gitopsModule.name + ' [' + gitopsModule.version + ']';
            } else {
                suggestion.detail = gitopsModule.name;
            }
            suggestion.detail += ' (Gitops)';
            var doc = '**Name**: ' + gitopsModule.name;
            if (gitopsModule.version) {
                doc += '\n\n**Version**: ' + gitopsModule.version;
            }
            if (gitopsModule.author) {
                doc += '\n\n**Author**: ' + gitopsModule.author;
            }
            if (gitopsModule.description) {
                doc += '\n\n**Description**: ' + gitopsModule.description;
            }
            suggestion.documentation = new vscode.MarkdownString(doc);
            suggestions.push(suggestion);
        }
        if (suggestions.length >= 20) {
            return suggestions;
        }
    }
    if (gsysModules[projectDir]) {
        for (const gsysModule of gsysModules[projectDir]) {
            if (!prefix || gsysModule.name.startsWith(prefix)) {
                var suggest = new vscode.CompletionItem(gsysModule.name);
                suggest.kind = vscode.CompletionItemKind.Module;
                suggest.insertText = gsysModule.fullName;
                suggest.detail = gsysModule.fullName;
                suggest.documentation = gsysModule.path;
                suggestions.push(suggest);
            }
            if (suggestions.length >= 100) {
                return suggestions;
            }
        }
    }
    return suggestions;
}

export async function initImports() {
    if (vscode.workspace.workspaceFolders) {
        await await initGitopsModules(vscode.workspace.workspaceFolders[0].uri.fsPath);
    }

    if (isProjectMode()) {
        for (const project of getProjects()) {
            await initGsysDirectories(project.wsFolder.uri.fsPath, project.filePath);
        }
    } else {
        if (vscode.workspace.workspaceFolders) {
            await initGsysDirectories(vscode.workspace.workspaceFolders[0].uri.fsPath, '');
        }
    }
}

export async function addFileToImports(file: string) {
    if (isProjectMode()) {
        for (const project of getProjects()) {
            let projectDir = project.wsFolder.uri.fsPath;
            if (file.startsWith(projectDir)) {
                if (!gsysModules[projectDir]) {
                    gsysModules[projectDir] = [];
                }
                gsysModules[projectDir].push(createGsysModule(projectDir, projectDir, path.dirname(file), path.basename(file)));
            }
        }
    } else {
        if (vscode.workspace.workspaceFolders) {
            let projectDir = vscode.workspace.workspaceFolders[0].uri.fsPath;
            if (!gsysModules[projectDir]) {
                gsysModules[projectDir] = [];
            }
            gsysModules[projectDir].push(createGsysModule(projectDir, projectDir, path.dirname(file), path.basename(file)));
        }
    }
}

export async function removeFileFromImports(file: string) {
    for (const key in gsysModules) {
        const items = gsysModules[key];
        var i = 0;
        while (i < items.length) {
            if (items[i].path === file) {
                items.splice(i);
            } else {
                i++;
            }
        }
    }
}