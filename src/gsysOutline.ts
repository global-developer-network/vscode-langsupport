/*---------------------------------------------------------
 * See LICENSE in the project root for license information.
 *--------------------------------------------------------*/

'use strict';

import vscode = require('vscode');

import { getDirtyFile } from './gsysUtils';
import { getFileSymbols, findWorkspaceSymbols } from './gsysIndexer';

export class GsysWorkspaceSymbolProvider implements vscode.WorkspaceSymbolProvider {

    public provideWorkspaceSymbols(query: string, token: vscode.CancellationToken): Thenable<vscode.SymbolInformation[]> {
        return findWorkspaceSymbols(query);
    }
}

export class GsysDocumentSymbolProvider implements vscode.DocumentSymbolProvider {
    public provideDocumentSymbols(document: vscode.TextDocument, token: vscode.CancellationToken): Thenable<vscode.SymbolInformation[]> {
        return getFileSymbols(document.fileName, getDirtyFile(document));
    }
}
