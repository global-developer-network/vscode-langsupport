/*---------------------------------------------------------
 * See LICENSE in the project root for license information.
 *--------------------------------------------------------*/

'use strict';

import vscode = require('vscode');

export const NIM_MODE: vscode.DocumentFilter = { language: 'gsys', scheme: 'file' };