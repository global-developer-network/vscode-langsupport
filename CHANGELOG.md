# ChangeLog

## 0.0.1 (28 Dec 2020)
- Created the boilerplate refactor of Nim's extension.   
- We will begin dropping in functionality after the Open API schema is finalized.
- Language Syntax is being documented in a separate repo for docs.stackstandard.com
- Language Playground will be made available through GitPod.
- Primary extensions are being dropped in currently into the Language kit which is in language.stackstandard.com
- Primary Language functionality will be from Nim, which supports single drop-in binaries and Node modules.