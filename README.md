# Gsys for Visual Studio Code

[![Version](https://vsmarketplacebadge.apphb.com/version/kosz78.gsys.svg)](https://marketplace.visualstudio.com/items?itemName=globalDeveloperNetwork.gsys)
[![Installs](https://vsmarketplacebadge.apphb.com/installs/kosz78.gsys.svg)](https://marketplace.visualstudio.com/items?itemName=globalDeveloperNetwork.gsys)
[![Ratings](https://vsmarketplacebadge.apphb.com/rating/kosz78.gsys.svg)](https://vsmarketplacebadge.apphb.com/rating/globalDeveloperNetwork.gsys.svg)
[![Build Status](https://travis-ci.org/GlobalDeveloperNetwork/vscode-gsys.svg?branch=master)](https://travis-ci.org/GlobalDeveloperNetwork/vscode-gsys)

This extension adds language support for the GitOps.Systems language to VS Code, including:

- Syntax Highlight (gsys, gitops, gsys.cfg, .stack)
- Code Completion
- Signature Help
- Goto Definition
- Find References
- File outline
- Build-on-save
- Workspace symbol search
- Quick info
 
## Using

First, you will need to install Visual Studio Code `0.10`.
In the command palette (`cmd-shift-p`) select `Install Extension` and choose `Gsys`.

The following language is specifically supported by this extension.
* Gsys Language - http://language.stackstandard.com

_Note_: It is recommended to turn `Auto Save` on in Visual Studio Code (`File -> Auto Save`) when using this extension.

### Options

The following Visual Studio Code settings are available for the Gsys extension.  These can be set in user preferences (`cmd+,`) or workspace settings (`.vscode/settings.json`).
* `gsys.buildOnSave` - perform build task from `tasks.json` file, to use this options you need declare build task according to [Tasks Documentation](https://code.visualstudio.com/docs/editor/tasks), for example:
	```json
	{
	   "taskName": "Run module.gsys",
	   "command": "gsys",
	   "args": ["c", "-o:bin/${fileBasenameNoExtension}", "-r", "${fileBasename}"],
	   "options": {
	      "cwd": "${workspaceRoot}"
	   },
	   "type": "shell",
	   "group": {
	      "kind": "build",
	      "isDefault": true
	   }
	}
	```
* `gsys.lintOnSave` - perform the project check for errors on save
* `gsys.project` - optional array of projects file, if gsys.project is not defined then all gsys files will be used as separate project
* `gsys.licenseString` - optional license text that will be inserted on gsys file creation


#### Example

```json
{
	"gsys.buildOnSave": false,
	"gsys.buildCommand": "c",
	"gsys.lintOnSave": true,
	"gsys.project": ["project.gsys", "project2.gsys"],
	"gsys.licenseString": "# Copyright 2020 GitOps.Systems for Full Stack Engine, powered by a Global Developer Network and LMG.\n\n"
}
```

### Commands
The following commands are provided by the extension:

* `Gsys: Run selected file` - compile and run selected file, it uses `c` compiler by default, but you can specify `cpp` in `gsys.buildCommand` config parameter.
This command available from file context menu or by `F6` keyboard shortcut.

## TODO

* Rename support
* Debug support

## ChangeLog

ChangeLog is located [here](https://github.com/GlobalDeveloperNetwork/vscode-gsys/blob/master/CHANGELOG.md)

